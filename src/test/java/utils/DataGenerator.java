package utils;


import io.qameta.allure.Allure;
import io.qameta.allure.Step;

import java.text.SimpleDateFormat;
import java.util.Random;

public class DataGenerator {

    @Step
    public static int getRandomExistentID() {
        Random random = new Random();
        int result = random.nextInt(1, 13);
        Allure.step("Generated random existent ID: " + result);
        return result;
    }

    public static int getRandomNonExistentID() {
        Random random = new Random();
        int result = random.nextInt(13, Integer.MAX_VALUE);
        Allure.step("Generated random nonexistent ID: " + result);
        return result;
    }

    public static String getRandomName() {
        String actualTime = getActualTime();
        String result = "randomName" + actualTime;

        Allure.step("Generated random name: " + result);
        return result;
    }

    public static String getRandomPassword() {
        String actualTime = getActualTime();
        String result = "randomPassword" + actualTime;

        Allure.step("Generated random password: " + result);
        return result;
    }

    public static String getRandomJob() {
        String actualTime = getActualTime();
        String result = "randomJob" + actualTime;

        Allure.step("Generated random job: " + result);
        return result;
    }

    private static String getActualTime() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date());
    }

}
