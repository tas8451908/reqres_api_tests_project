package utils;

import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class Assertions {

    public static void assertResponseCodeEquals(Response response, int expectedStatusCode) {
        assertEquals(expectedStatusCode,
                response.statusCode(), "Response status code is not as expected");
    }

    // Possible improvement: Add assertion which will check
    // that key exists in JSON no matter the level of nesting
    public static void assertUsersJsonHasKey(Response response, String expectedFieldName) {
        response.then().assertThat().body("data", hasKey(expectedFieldName));
    }

    public static void assertJsonHasKey(Response response, String expectedFieldName) {
        response.then().assertThat().body("$", hasKey(expectedFieldName));
    }

    public static void assertJsonHasKeyWithValue(Response response, String key, String expectedValue) {
        String value = response.jsonPath().getString(key);
        assertEquals(expectedValue, value,
                "JSON value is not equal to expected value");
    }

    public static void assertResponseTextEquals(Response response, String expectedAnswer) {
        response.then().assertThat().body(equalTo(expectedAnswer));
    }

    public static void assertTimeLessThen(Response response, long l) {
        response.then().assertThat().time(lessThan(l));
    }
}
