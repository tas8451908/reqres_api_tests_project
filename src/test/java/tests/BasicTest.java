package tests;
import org.junit.jupiter.api.BeforeAll;
import utils.DataGenerator;
import io.restassured.RestAssured;
import java.util.HashMap;
import java.util.Map;

public class BasicTest {

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://reqres.in/api";
    }

    static int existentID = DataGenerator.getRandomExistentID();
    int notexistentID = DataGenerator.getRandomNonExistentID();
    Map<String, Object> editData = new HashMap<>();
}
