package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.HashMap;
import java.util.Map;

import static utils.Assertions.*;
import static utils.DataGenerator.getRandomJob;
import static utils.DataGenerator.getRandomName;

@Epic(value = "Create cases")

public class CreateTests extends BasicTest {

    Map<String, Object> data = new HashMap<>();
    static int userIDAfterCreation;

    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test create user")
    @DisplayName("Create user")
    @Tag("Positive")
    public void createUserTest() {

        String name = getRandomName();
        String job = getRandomJob();

        data.put("name", name);
        data.put("job", job);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(data)
                .when()
                .post("/users");

        assertResponseCodeEquals(response, 201);
        assertJsonHasKeyWithValue(response, "name", name);
        assertJsonHasKeyWithValue(response, "job", job);
        assertJsonHasKey(response, "id");
        assertJsonHasKey(response, "createdAt");

        userIDAfterCreation = response.jsonPath().getInt("id");
    }

    @ParameterizedTest(name = "({argumentsWithNames})")
    @Owner("Anastasiia Shulgina")
    @Description("This test create user with different params")
    @DisplayName("Create user with different types of data")
    @Tag("Positive")
    @CsvSource({
            "John Doe, Software Developer",
            "12345, 567890",
            "!·%, )(*?:%;",
            "Павел, повар",
            "张三, 软件开发工程师",
            "田中太郎, プロジェクトマネージャー"
    })
    public void createUserTestWithParams(String name, String job) {
        data.put("name", name);
        data.put("job", job);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(data)
                .when()
                .post("/users");

        assertResponseCodeEquals(response, 201);
        assertJsonHasKeyWithValue(response, "name", name);
        assertJsonHasKeyWithValue(response, "job", job);
        assertJsonHasKey(response, "id");
        assertJsonHasKey(response, "createdAt");

        userIDAfterCreation = response.jsonPath().getInt("id");

    }


    @AfterEach
    public void deleteUserAfterCreation() {

        Response response = RestAssured.delete("/users/" + userIDAfterCreation);

        if (response.getStatusCode() != 204) {
            throw new AssertionError("User wasn't deleted. Status code: " + response.getStatusCode());
        }

    }
}
