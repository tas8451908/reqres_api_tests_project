package tests;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import io.qameta.allure.*;

import static utils.Assertions.*;

@Epic(value = "Read cases")
public class ReadTests extends BasicTest {

    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test successfully get email, first name, last name and link with avatar by id")
    @DisplayName("Get user info")
    @Tag("Positive")
    public void getUserByIDTest() {

        Response response = RestAssured
                .get("/users/" + existentID);
        assertResponseCodeEquals(response, 200);


        assertUsersJsonHasKey(response, "id");
        assertUsersJsonHasKey(response, "email");
        assertUsersJsonHasKey(response, "first_name");
        assertUsersJsonHasKey(response, "last_name");
        assertUsersJsonHasKey(response, "avatar");

    }

    //неготивный тест: задаем ИД несуществующего юзера - возвращется код 404
    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test send get request with nonexistent id and receive 404 code ")
    @DisplayName("Get info of nonexistent user")
    @Tag("Negative")
    public void getUserByNonExistentIDTest() {
        Response response = RestAssured
                .get("/users/" + notexistentID);
        assertResponseCodeEquals(response, 404);
    }


    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test successfully get info about list og users")
    @DisplayName("Get info of all users")
    @Tag("Positive")
    public void getUsersInfoTest() {
        Response response = RestAssured
                .get("/users/");

        assertResponseCodeEquals(response, 200);
    }

    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test successfully get info about user with delay less then 4 sec and make logs of sending params and response in case of fail")
    @DisplayName("Get info of user with delay")
    @Tag("Positive")
    public void getUserInfoMethodWithDelay() {
        Response response = RestAssured
                .given().log().params()
                .when()
                .get("/users?delay=3")
                .then()
                .log().ifStatusCodeIsEqualTo(400)
                .extract()
                .response();

        assertResponseCodeEquals(response, 200);
        assertTimeLessThen(response, 4000L);

    }

}
