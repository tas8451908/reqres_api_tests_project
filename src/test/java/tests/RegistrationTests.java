package tests;

import org.junit.jupiter.api.Tag;

import io.qameta.allure.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;


import java.util.HashMap;
import java.util.Map;

import static utils.Assertions.*;
import static utils.DataGenerator.getRandomPassword;

@Epic(value = "Registration cases")

public class RegistrationTests extends BasicTest {
    Map<String, Object> registrationData = new HashMap<>();
    static String email;

    @BeforeEach
    public void getRandomExistentEmail() {

        Response response = RestAssured
                .get("/users/" + existentID);
        assertResponseCodeEquals(response, 200);

        email = response.jsonPath().getString("data.email");

    }

    @Owner("Anastasiia Shulgina")
    @Description("This test check user registration with correct data")
    @DisplayName("User registration")
    @Tag("Positive")
    @Test
    public void registerUserCorrectTest() {

        String password = getRandomPassword();

        registrationData.put("email", email);
        registrationData.put("password", password);


        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(registrationData)
                .when()
                .post("/register");


        assertResponseCodeEquals(response, 200);
        assertJsonHasKey(response, "id");
        assertJsonHasKey(response, "token");
    }

    @Test
    @Description("This test check that user can't register without password")
    @DisplayName("User registration without password")
    @Tag("Negative")
    public void registerUserWithoutPasswordTest() {

        registrationData.put("email", email);


        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(registrationData)
                .when()
                .post("/register");


        assertResponseCodeEquals(response, 400);
    }

    @Test
    @Description("This test check that user can't register with empty password")
    @DisplayName("User registration with empty password")
    @Tag("Negative")
    public void registerUserWithEmptyPasswordTest() {

        registrationData.put("email", email);
        registrationData.put("password", "");


        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(registrationData)
                .when()
                .post("/register");

        assertResponseCodeEquals(response, 400);
        assertResponseTextEquals(response, "{\"error\":\"Missing password\"}");
    }
}
