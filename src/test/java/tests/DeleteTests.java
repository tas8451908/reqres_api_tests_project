package tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;


import java.util.HashMap;
import java.util.Map;

import static utils.Assertions.assertResponseCodeEquals;
import static utils.DataGenerator.getRandomJob;
import static utils.DataGenerator.getRandomName;

@Epic(value = "Delete cases")
public class DeleteTests extends BasicTest {
    static Map<String, Object> data = new HashMap<>();

    static int userIDtoDelete;

    @BeforeEach
    public void createUserBeforeDelete() {
        String name = getRandomName();
        String job = getRandomJob();

        data.put("name", name);
        data.put("job", job);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(data)
                .when()
                .post("/users");

        assertResponseCodeEquals(response, 201);

        userIDtoDelete = response.jsonPath().getInt("id");
    }


    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test delete user")
    @DisplayName("Delete user")
    @Tag("Positive")
    public void deleteUserTest() {

        Response response = RestAssured
                .delete("/users" + userIDtoDelete);

        assertResponseCodeEquals(response, 204);

    }
}
