package tests;

import org.junit.jupiter.api.Tag;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import static utils.Assertions.*;
import static utils.DataGenerator.getRandomJob;
import static utils.DataGenerator.getRandomName;

@Epic(value = "Update cases")

public class UpdateTests extends BasicTest {


    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test update user data by PUT method")
    @DisplayName("Update user with PUT")
    @Tag("Positive")
    public void updateUserWithPutTest() {
        String updatedName = getRandomName();
        String updatedJob = getRandomJob();

        editData.put("name", updatedName);
        editData.put("job", updatedJob);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(editData)
                .when()
                .put("/users/" + existentID);

        assertResponseCodeEquals(response, 200);
        assertJsonHasKeyWithValue(response, "name", updatedName);
        assertJsonHasKeyWithValue(response, "job", updatedJob);
        assertJsonHasKey(response, "updatedAt");
    }

    @Test
    @Owner("Anastasiia Shulgina")
    @Description("This test update user data by PATCH method")
    @DisplayName("Update user with PATCH")
    @Tag("Positive")
    public void updateUserWithPatchTest() {
        String updatedName = getRandomName();
        String updatedJob = getRandomJob();

        editData.put("name", updatedName);
        editData.put("job", updatedJob);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(editData)
                .when()
                .patch("/users/" + existentID);

        assertResponseCodeEquals(response, 200);
        assertJsonHasKeyWithValue(response, "name", updatedName);
        assertJsonHasKeyWithValue(response, "job", updatedJob);
        assertJsonHasKey(response, "updatedAt");
    }
}
