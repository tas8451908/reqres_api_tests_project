
## Demonstration API test automation project for <a href="https://reqres.in/ ">reqres.in</a>

This project was created for educational proposes.
The main idea was to practice way of writing API auto tests using public API of [Reqres.in](https://reqres.in/)

### Technology stack:

- [x] Java 17

- [x] Rest Assured for API tests

- [x] Allure Report generates a test run report

- [x] Maven is used for automated project build

- [x] JUnit 5 is used as a unit testing library


### Run tests and generate reports:
**Running tests locally**

  `mvn clean test`

**Create report**

  `mvn allure:serve`

### Opportunities of [REST Assured](https://github.com/rest-assured/rest-assured/wiki/Usage) used in the project:

* Specifying Request Data (ex. body with data, by creating JSON by supplying a Map)
* Extracting values from the Response with JsonPath/has key
* Verifying Response Data with assert.that
* Measuring Response Time (in test with delay)
* Logging (ex params for request and all data from response in case of 400 Status Code)

### Opportunities of [Allure](https://allurereport.org/docs/) used in the project:

* Description, links and other metadata: @Description @Owner @Tag
* Test steps: I used @Step to show in Report with what data tests were run
* Inegration with Junit4, and then migration to Junit5

Main page of Allure-report
<div style="text-align:center">
<img src="./src/main/images/screens/AllureMain.PNG" title="Allure Overview Dashboard" alt="Allure Overview Dashboard"/>
</div>

Suites page of Allure-report
<div style="text-align:center">
<img src="./src/main/images/screens/AllureSuites.PNG" title="Suites page of Allure-report" alt="Suites page of Allure-report"/>
</div>


**Ideas for future improvements to the project:**
* JSON schema validation
* Addition annotation to improve Allure report
* GitLab/TeamCity runs the tests.
